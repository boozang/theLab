import Vue from 'vue'
import Router from 'vue-router'
import VeeValidate from 'vee-validate';
 
import labHome from '../components/labHome.vue'
import formOne from '../components/labForms/formOne.vue'
import listOne from '../components/labLists/listOne.vue'
import listTwo from '../components/labLists/listTwo.vue'
import concatStr from  '../components/labStrings/concatStr.vue'

import rightBtn from  '../components/labConditionals/rightBtn.vue'
import catOrDog from  '../components/labConditionals/catOrDog.vue'

import tooFast from   '../components/labWaits/tooFast.vue'
import fastest from   '../components/labWaits/fastest.vue'


Vue.use(Router);
Vue.use(VeeValidate);

export default new Router({
  routes: [
    { path: '/', name: labHome, component:labHome },
    { path: '/labForms/formOne', component:formOne },
    { path: '/labLists/listOne', component:listOne },
    { path: '/labLists/listTwo', component:listTwo },
    { path: '/labStrings/concatStr', component:concatStr },
      
    { path: '/labConditionals/rightBtn', component:rightBtn },
    { path: '/labConditionals/catOrDog', component:catOrDog },
      
    { path: '/labWaits/tooFast.vue', component:tooFast },
    { path: '/labWaits/fastest.vue', component:fastest }
      
  ],
     mode: 'history'
})
